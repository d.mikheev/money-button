package ru.titus;

import com.binance.api.client.BinanceApiAsyncRestClient;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.BinanceApiWebSocketClient;
import com.binance.api.client.domain.market.CandlestickInterval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Core {

    @Autowired
    BinanceApiAsyncRestClient asyncRestClient;
    @Autowired
    BinanceApiRestClient syncRestClient;
    @Autowired
    BinanceApiWebSocketClient webSocketClient;

    public void getAndSaveLastTrades() {
        asyncRestClient.ping(response -> System.out.println(response));
        asyncRestClient.getServerTime(response -> System.out.println(response));
    }

    public void getAndSaveOrderBook() {
        asyncRestClient.getOrderBook("BTCUSDT",20, response -> System.out.println(response));
    }

    public void startMonitoring() {

        String listenKey = syncRestClient.startUserDataStream();


        // It illustrates how to create a stream to obtain all market tickers.
//        webSocketClient.onAllMarketTickersEvent(event -> {
//            System.out.println("All market "+event);
//        });
//
//        // Listen for aggregated trade events for ETH/BTC
//        webSocketClient.onAggTradeEvent("btcusdt", response -> System.out.println("Agg trade "+response));
//
//        // Listen for changes in the order book in ETH/BTC
//        webSocketClient.onDepthEvent("btcusdt", response -> System.out.println("On depth "+response));


        // Obtain 1m candlesticks in real-time for ETH/BTC
        webSocketClient.onCandlestickEvent("btcusdt", CandlestickInterval.ONE_MINUTE, response -> System.out.println(response));
       // webSocketClient.onUserDataUpdateEvent()
    }

}
