package ru.titus.configuration;

import com.binance.api.client.BinanceApiAsyncRestClient;
import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.BinanceApiWebSocketClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;

@Configuration
@PropertySource("classpath:/app.properties")
public class BinanceAPIConfig {

    @Autowired Environment env;

    private BinanceApiClientFactory binanceApiClientFactory;

    @PostConstruct
    private void init(){
        binanceApiClientFactory = BinanceApiClientFactory.newInstance(env.getProperty("api.public"),env.getProperty("api.secret"));
    }

    @Bean
    public BinanceApiAsyncRestClient createBinanceAsyncApiClient(){
        return binanceApiClientFactory.newAsyncRestClient();
    }

    @Bean
    public BinanceApiRestClient createBinanceSyncApiClient(){
        return binanceApiClientFactory.newRestClient();
    }

    @Bean
    public BinanceApiWebSocketClient createBinanceWebsocketApiClient(){
        return binanceApiClientFactory.newWebSocketClient();
    }

}
