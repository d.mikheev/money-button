package ru.titus.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.titus.Core;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    @Autowired Core core;

    @RequestMapping(value="/test", method= RequestMethod.GET)
    public void test(){
        core.getAndSaveLastTrades();
        core.startMonitoring();
    }

    @RequestMapping(value="/book", method= RequestMethod.GET)
    public void book(){
        core.getAndSaveOrderBook();
    }

}
